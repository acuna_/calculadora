
document.getElementById("uno").addEventListener("click",uno);
document.getElementById("dos").addEventListener("click",dos);
document.getElementById("tres").addEventListener("click",tres);
document.getElementById("cuatro").addEventListener("click",cuatro);
document.getElementById("cinco").addEventListener("click",cinco);
document.getElementById("seis").addEventListener("click",seis);
document.getElementById("siete").addEventListener("click",siete);
document.getElementById("ocho").addEventListener("click",ocho);
document.getElementById("nueve").addEventListener("click",nueve);
document.getElementById("cero").addEventListener("click",cero);
document.getElementById("suma").addEventListener("click",suma);
document.getElementById("resta").addEventListener("click",resta);
document.getElementById("multiplicacion").addEventListener("click",multiplicacion);
document.getElementById("division").addEventListener("click",division);
document.getElementById("porcentaje").addEventListener("click",porcentaje);
document.getElementById("igual").addEventListener("click",finResultado);
document.getElementById("borrar").addEventListener("click",borrar);

function uno(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("uno").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function dos(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("dos").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function tres(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("tres").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function cuatro(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("cuatro").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function cinco(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("cinco").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function seis(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("seis").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function siete(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("siete").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function ocho(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("ocho").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function nueve(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("nueve").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function cero(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("cero").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ sumado
}

function suma(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("suma").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ " " + sumado +" "
}

function resta(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("resta").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ " " + sumado +" "
}

function multiplicacion(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("multiplicacion").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ " " + sumado +" "
}

function division(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("division").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ " " + sumado +" "
}

function porcentaje(){
  let actual = document.getElementById("resultado").innerHTML;
  let sumado = document.getElementById("porcentaje").innerHTML;
  document.getElementById("resultado").innerHTML = actual+ " " + sumado +" "
}

function finResultado() {

  let actual = document.getElementById("resultado").innerHTML;
  let sumar = actual.indexOf("+");
  let restar = actual.indexOf("-");
  let mult = actual.indexOf("*");
  let div = actual.indexOf ("/");
  let porc = actual.indexOf ("%");

  if (sumar !== -1) {
		arr = actual.split("+",2);
		res = parseInt(arr[0]) + parseInt(arr[1]);
    document.getElementById("resultado").innerHTML = res;
    
	} else if (restar !== -1) {
		arr = actual.split("-",2);
		res = arr[0] - arr[1];
		document.getElementById("resultado").innerHTML = res;
		
	} else if (div !== -1) {
		arr = actual.split("/",2);
		res = arr[0] / arr[1];
		document.getElementById("resultado").innerHTML = res;
		
	} else if (mult !== -1) {
		arr = actual.split("*",2);
		res = arr[0] * arr[1];
		document.getElementById("resultado").innerHTML = res;
		
  }else if (porc !== -1) {
		arr = actual.split("%",2);
		res = arr[0] % arr[1];
    document.getElementById("resultado").innerHTML = res;
  }
}

function borrar(){
  let a = " ";
  document.getElementById("resultado").innerHTML = a;
}